

all:
	./bin/install

example:
	./run-compare \
		-tsv input/latex_from_qrels/ \
		-r input/example_run_files/ \
		-q input/qrels/qrel_task2_2021_test_official_evaluation.tsv \
		-o output/index.html \
		-t input/topics/Topics_Task2_2021_V1.1.xml

# the referenced tsv files only live on bob they are too big to store in the repo
compare-phoc-2020:
	./run-compare \
		-tsv /home/devops/arqmathdata_unique \
		-r input/2020_topics_arqmath_task2_runs/ \
		-q input/qrels/ TODO \
		-o output/index-2020-task2-phoc.html \
		-t input/topics/Topics_V1.1.xmlc

compare-phoc-2021:
	./run-compare \
		-tsv /home/mlang/Downloads/te3st/ \
		-r input/2021_topics_arqmath_task2_runs \
		-q input/qrels/qrel_task2_2021_test_official_evaluation.tsv \
		-o output/index-2021-task2-phoc.html \
		-t input/topics/Topics_Task2_2021_V1.1.xml

compare-phoc-visu-2021:
	./run-compare \
		-tsv /home/mattlangsenkamp/Documents/dprl/arqmathdata_unique \
		-r input/visu-runs-2021 \
		-q input/qrels/qrel_task2_2021_test_official_evaluation.tsv \
		-o output/index-2021-task2-visu-phoc.html \
		-t input/topics/Topics_Task2_2021_V1.1.xml

compare-phoc-2022:
	./run-compare \
		-tsv /home/mlang/Downloads/latex_representation_v3/ \
		-r input/2022_topics_arqmath_task2_runs \
		-q input/qrels/qrel_task2_2022_official.tsv \
		-o output/index-2022-task2-phoc.html \
		-t input/topics/Topics_Task2_2022_V0.1.xml \
		-na 2022_arqmath_task2

compare-phoc-2022-select:
	./run-compare \
		-tsv /home/mlang/Downloads/latex_representation_v3/ \
		-r input/2022_select \
		-q input/qrels/qrel_task2_2022_official.tsv \
		-o output/index-2022-task2-select.html \
		-t input/topics/Topics_Task2_2022_V0.1.xml \
		-na 2022_arqmath_task2 \
		-n 5