import argparse
import os
from datetime import datetime
import bs4
from pyspark.sql import SparkSession, Window
import argparse
import time
from tqdm import tqdm

from anyphoc.canvas.converters.svgcanvas import render
from pyspark.sql.functions import col, rank, first
from pyspark.sql.types import StructType, StructField, StringType, IntegerType, FloatType
from anyphoc.canvas.converters.svgcanvas import render as ap_render
from udfs.render import render_svg
from vis.visualize import start_html, add_hit_to_html


def main(spark, args):

    schema = StructType([
        StructField("qid", StringType(), True),
        StructField("formula_id", StringType(), True),
        StructField("post_id", IntegerType(), True),
        StructField("rank", IntegerType(), True),
        StructField("score", FloatType(), True),
        StructField("run_id", StringType(), True)
    ])

    qrel_schema = StructType([
        StructField("qid", StringType(), True),
        StructField("zero", IntegerType(), True),
        StructField("id", IntegerType(), True),
        StructField("relevance", FloatType(), True)
    ])

    qrel_df = spark.read \
        .schema(qrel_schema) \
        .option("mode", "DROPMALFORMED") \
        .csv(args.qrel, sep=r'\t')
    print(f"qrel size: {qrel_df.count()}")
    # get just the hits w

    run_df = spark.read \
        .schema(schema) \
        .option("mode", "DROPMALFORMED") \
        .option("multiLine", True) \
        .option("escape", "\"") \
        .csv(args.runs_dir, sep=r'\t')
    print(f"runfiles size: {run_df.count()}")

    # get the latex representation tsv
    input_df = spark.read \
        .option("inferSchema", True) \
        .option("header", True) \
        .option("mode", "DROPMALFORMED") \
        .option("multiLine", True) \
        .option("escape", "\"") \
        .csv(args.tsv_dir, sep=r'\t')
    print(f"formula tsv size: {input_df.count()}")
    # attaching visual_id to run files"
    joined_df = run_df \
        .join(input_df.select("id", "visual_id", "formula"), run_df.formula_id == input_df.id, 'inner')

    qrel_df_new = qrel_df.select(col("id").alias("id_a"), col("relevance"), col("qid").alias("qid_a"))

    joined_df = joined_df.join(
        qrel_df_new,
        #["qid", "id"],
        (col("visual_id") == col("id_a")) & (col("qid") == col("qid_a")),  # don't want the relevance from other query
        'inner')

    # drop duplicates
    window = Window.partitionBy(col("run_id"), col("qid"), col("visual_id")).orderBy(col("rank").asc())
    joined_df = joined_df.select('*', rank().over(window).alias("rank2")).filter(col("rank2") == 1) \
        .sort(col("qid"), col("run_id"), col("rank")).drop("rank2")

    # get only the first N non-duplicate hits
    window = Window.partitionBy(col('qid'), col('run_id')).orderBy(col('rank').asc())
    joined_df = joined_df \
        .select('*', rank()
                .over(window) \
                .alias('rank2')) \
        .filter(col('rank2') <= int(args.num_hits)).drop("rank2") \

    # attaching formula to qrel
    joined_qrel = input_df.select("formula", "visual_id").join(qrel_df, qrel_df.id == input_df.visual_id, 'inner') \
        .drop_duplicates(["qid", "visual_id"])

    # rendered_df = joined_df.withColumn("rendered_formula",
    #                                   render_svg("http://localhost:8045/render")(col("formula")))
    # rendered_qrel = joined_qrel.withColumn("rendered_formula",
    #                                       render_svg("http://localhost:8045/render")(col("formula")))

    # print("rendered df size", rendered_df.count())
    # print("rendered qrel size", rendered_qrel.count())
    out_dic = dict()

    collected = joined_df.collect()
    for hit in tqdm(collected):
        if hit.qid in out_dic.keys():
            q_dict = out_dic[hit.qid]
        else:
            out_dic[hit.qid] = {}
            q_dict = out_dic[hit.qid]
        try:
            rendered_formula = ap_render(hit.formula.strip(), "http://localhost:8045/render")
        except Exception as e:
            print(e)
            rendered_formula = None
        if hit.run_id in q_dict.keys():
            q_dict[hit.run_id].append(
                {"id": hit.id,
                 "rank": hit.rank,
                 "score": hit.score,
                 "visual_id": hit.visual_id,
                 "relevance": hit.relevance,
                 "rendered_formula": rendered_formula})
        else:
            q_dict[hit.run_id] = [
                {"id": hit.id,
                 "rank": hit.rank,
                 "score": hit.score,
                 "visual_id": hit.visual_id,
                 "relevance": hit.relevance,
                 "rendered_formula": rendered_formula}]

    with open(args.topic) as f:
        soup = bs4.BeautifulSoup(f.read(), 'xml')

    for hit in tqdm(joined_qrel.collect()):
        if hit.qid in out_dic.keys():
            q_dict = out_dic[hit.qid]
        else:
            out_dic[hit.qid] = {}
            q_dict = out_dic[hit.qid]
        try:
            rendered_formula = ap_render(hit.formula.strip(), "http://localhost:8045/render")
            if rendered_formula is None:
                time.sleep(20)
                print("ok trying again!")
                rendered_formula = ap_render(hit.formula.strip(), "http://localhost:8045/render")
        except Exception as e:
            print(e)
            time.sleep(10)
            print("ok trying again!")
            try:
                rendered_formula = ap_render(hit.formula.strip(), "http://localhost:8045/render")
            except Exception as e:
                print(e)
                rendered_formula = None

        if "IDEAL_FROM_QREL" in q_dict.keys():
            q_dict["IDEAL_FROM_QREL"].append(
                {"id": hit.id,
                 "relevance": hit.relevance,
                 "visual_id": hit.visual_id,
                 "rendered_formula": rendered_formula})
        else:
            q_dict["IDEAL_FROM_QREL"] = [
                {"id": hit.id,
                 "relevance": hit.relevance,
                 "visual_id": hit.visual_id,
                 "rendered_formula": rendered_formula}]

    '''for q in out_dic.keys():
        print(q)
        for r in out_dic[q].keys():
            print(f"\t{r}")'''

    vis_html = start_html(args.name + " run at: " + str(datetime.now()))
    for q in sorted(out_dic.keys()):
        query_latex = soup.find("Topic", {"number": q}).find("Latex").text
        query_svg = render(query_latex, "http://localhost:8045/render")
        add_hit_to_html(
            vis_html, q, out_dic[q], int(args.num_hits), query_svg)

    html = vis_html.prettify("utf-8")
    with open(args.out_file, "wb") as file:
        file.write(html)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-r", "--runs_dir")
    parser.add_argument("-q", "--qrel")
    parser.add_argument('-o', "--out_file")
    parser.add_argument('-tsv', "--tsv_dir")
    parser.add_argument('-t', "--topic", default="input/topics/Topics_Task2_2021_V1.1.xml")
    parser.add_argument('-n', "--num_hits", default=10)
    parser.add_argument('-na', "--name", default="result comparisons")
    parsed_args = parser.parse_args()

    spark_session = SparkSession.builder \
        .appName("comp") \
        .master("local[*]") \
        .config("spark.driver.memory", "120g") \
        .config("spark.executor.memory", "120g") \
        .getOrCreate()
    main(spark_session, parsed_args)
