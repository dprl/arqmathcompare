# Arqmathcompare
**Authors:** Matt Langsenkamp 
([Document and Pattern Recognition Lab (dprl)](https://www.cs.rit.edu/~dprl/index.html), Rochester Institute of Technology, USA)

**Purpose:** This tool exists to aid in the analysis of runs submitted to the arqmath competition [ARQMath](https://www.cs.rit.edu/~dprl/ARQMath/)

## Installation

**Requirements**

1. Python
2. Conda (e.g., from Anaconda) and pip for Python environments

Once you have these requirements installed, from the top directory of the project, you run the installation script by issuing:

```zsh
$ make
```
This will install all modules, and create a bash script named `run-compare` which can be used to run the tool

## Usage
First start a mathjax render service in the background with the following command: `docker run -d -p 8045:8081 dprl/mathjax-render`

Run the following script with the paths replaced by the files you would like to visualize. `-tsv` is path to full visually unique dataset. This can be obtained through phocindexing.
`-r` is directory containing ARQMath task 2 run files. Note these are in ARQMath format and have not yet been de-duplicated.
```commandline
./run-compare -tsv ../phocindexing/inputs/visu/ \
-r input/CIKMx5y3r9/ \
-q input/qrels/qrel_task2_2022_official.tsv \
-o output/index.html \
-t input/topics/Topics_Task2_2022_V0.1.xml
```


then open `output/index.html` in a browser. You will see each a list of queries which can be toggled. Each query will contain a list of the top `N` hits.
They will be color coded to indicate their relevance as described by the QREL file. Dark green means most relevant(3), light green means next most relevant(2), then orange(1), and red meaning not relevant(0).


## Support

This material is based upon work supported by the National Science Foundation (USA) under Grant Nos. IIS-1016815, IIS-1717997, and 2019897 (MMLI), and the Alfred P. Sloan Foundation under Grant No. G-2017-9827.

Any opinions, findings and conclusions or recommendations expressed in this material are those of the author(s) and do not necessarily reflect the views of the National Science Foundation or the Alfred P. Sloan Foundation.
