import bs4
from typing import Dict


def make_table_entry_ideal(hit: Dict):
    if hit["rendered_formula"] is None:
        hit["rendered_formula"] = "failed to render"
    entry = ['<div class="entry">',
                 '<div class="formula">',
                     hit["rendered_formula"],
                 "</div>",
                 '<div class="hit-meta">',
                     "<div>",
                         f"relevance: {hit['relevance']}",
                     "</div>",
                     "<div>",
                        f"visual_id: {hit['visual_id']}",
                     "</div>"
                 "</div>",
             "</div>"]

    return "\n".join(entry)


def make_table_entry(hit: Dict):
    if hit["rendered_formula"] is None:
        hit["rendered_formula"] = "failed to render"
    entry = ['<div class="entry">',
                 '<div class="formula">',
                    hit["rendered_formula"],
                 "</div>",
                 '<div class="hit-meta">',
                     "<div>",
                        f"score: {round(hit['score'], 3)}",
                     "</div>",
                     "<div>",
                        f"rank: {hit['rank']}",
                     "</div>",
                     "<div>",
                        f"visual_id: {hit['visual_id']}",
                     "</div>"
                 "</div>",
             "</div>"]

    return "\n".join(entry)


def make_table(q_dict: Dict, n: int) -> bs4.BeautifulSoup:
    table_parts = ['<div class="scrollit">',"<table>"]
    if "IDEAL_FROM_QREL" in q_dict.keys():
        ideal_list = q_dict["IDEAL_FROM_QREL"]
        ideal_list_sorted = sorted(ideal_list, key=lambda i: i['relevance'], reverse=True)
        table_parts.append("<tr>")
        table_parts.append(f'<td class="run-iden">IDEAL_FROM_QREL</td>')

        for hit in ideal_list_sorted[:n]:
            table_parts.append(f'<td class="{rel_map[hit["relevance"]]}">')
            table_parts.append(make_table_entry_ideal(hit))
            table_parts.append("</td>")
        table_parts.append("</tr>")
    for r in q_dict.keys():

        if r != "IDEAL_FROM_QREL":
            table_parts.append("<tr>")
            table_parts.append(f'<td class="run-iden">{r}</td>')
            r_hits = q_dict[r]
            r_hits_sorted = sorted(r_hits, key=lambda i: i['rank'])
            for hit in r_hits_sorted:
                table_parts.append(f'<td class="{rel_map[hit["relevance"]]}">')
                table_parts.append(make_table_entry(hit))
                table_parts.append("</td>")
            table_parts.append("</tr>")


    table_parts.append("</table>")
    table_parts.append("</div>")
    table = "".join(table_parts)
    return bs4.BeautifulSoup(table, "lxml")


rel_map = {
    0: "not-rel",
    1: "barely-rel",
    2: "semi-rel",
    3: "rel"
}