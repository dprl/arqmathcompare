import bs4
from vis.table_vis import make_table
from typing import Dict


def start_html(name: str) -> bs4.BeautifulSoup:
    return bs4.BeautifulSoup(template.format(name=name), "lxml")


def find_or_create_query(soup, qid: str, q_svg: str) -> bs4.Tag:
    collapsables = soup.find("div", {"id": "collapsables"})
    q_section = collapsables.find("div", {"id": qid, "class": "content"})
    if q_section:
        return q_section

    new_q_button = soup.new_tag("button")
    new_q_button["id"] = qid
    new_q_button["class"]="collapsable"
    new_q_button.string = qid
    new_q_div = soup.new_tag("div", id=qid)
    new_q_div["class"] = 'content'
    svg=bs4.BeautifulSoup(q_svg, "lxml").find('svg')
    svg["height"] = '100px'
    svg["width"] = '100%'
    query_tag = ['<div class="query-svg">',
                 '<div><b>Rendered Query:<b></div>',
                 str(svg.prettify()),
                 '</div>']
    new_q_div.append(bs4.BeautifulSoup("".join(query_tag), "lxml"))
    collapsables.append(new_q_button)
    collapsables.append(new_q_div)
    return collapsables.find("div", {"id": qid, "class": "content"})


def add_hit_to_html(soup: bs4.BeautifulSoup, qid, q_dict: Dict, n, query_svg):

    query_tag = find_or_create_query(soup, qid, query_svg)
    query_tag.append(make_table(q_dict, n))


template = r'''<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
.collapsible {{
  background-color: #777;
  color: white;
  cursor: pointer;
  padding: 18px;
  width: 100%;
  border: none;
  text-align: left;
  outline: none;
  font-size: 15px;
}}
table {{
    border:2px solid black;
}}

th, td {{
  padding: 15px;
  border: 2px solid black;
}}

.unranked {{
    border: 2px solid black;
}}

.rel {{
    border: 4px solid green;
}}

.semi-rel {{
    border: 4px solid yellowgreen;
}}

.barely-rel {{
    border: 4px solid orange;
}}

.not-rel {{
    border: 4px solid red;
}}

.query-svg {{
    padding: 15px;
}}
.active, .collapsible:hover {{
  background-color: #555;
}}
.entry {{
    display: flex;
    flex-direction: column;
    margin: 3px;
    min-width: 130px;
    justify-content: space-between;
}}
.hit-meta{{
    margin-top: 25px;
    
}}

.scrollit {{
    overflow:scroll;
}}

.run-iden {{
    font-size: 20px;
    font-weight: bold;
}}

.content {{
  padding: 0 18px;
  max-height: 0;
  overflow: hidden;
  transition: max-height 0.1s ease-out;
}}
</style>
</head>
<body>

<h2>{name}</h2>

<div id="collapsables">

</div>


<script>
var coll = document.getElementsByClassName("collapsable");
var i;

for (i = 0; i < coll.length; i++) {{
  coll[i].addEventListener("click", function() {{
    this.classList.toggle("active");
    var content = this.nextElementSibling;
    if (content.style.maxHeight){{
      content.style.maxHeight = null;
    }} else {{
      content.style.maxHeight = content.scrollHeight + "px";
    }} 
  }});
}}
</script>

</body>
</html>'''